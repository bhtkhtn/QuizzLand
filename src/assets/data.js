export const data = [
	{
		question: '23GFOqoj2i9erTv4AFk8uE0s2',
		position: 1,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 1.2324419562,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 0.3381309915,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 1.0055057296,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 0.5959353535,
				answer: 4,
			},
		],
	},
	{
		question: 'kYlFr2d923628H2Tq12e40sRKi',
		position: 2,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 1.0015200072,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 2.940591439,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 1.1227768999,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 1.6605601458,
				answer: 4,
			},
		],
	},
	{
		question: "constriction's",
		position: 3,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 0.3640397473,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 0.1638591676,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 1.8154542931,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 0.3246945514,
				answer: 4,
			},
		],
	},
	{
		question: '3nSo7a8J47Sw2u7R27Fzf1Z8G0',
		position: 4,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 4.3492226338,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 1.0322877995,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 0.3477764994,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 0.5077577184,
				answer: 4,
			},
		],
	},
	{
		question: "reconstruction's",
		position: 5,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 0.2600912116,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 0.7180169776,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 1.1140104337,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 1.3294031934,
				answer: 4,
			},
		],
	},
	{
		question: "decentralization's",
		position: 6,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 0.5253600588,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 2.8345522836,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 5.0567379682,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 0.070912319,
				answer: 4,
			},
		],
	},
	{
		question: "transfusion's",
		position: 7,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 1.4351376141,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 0.0481177084,
				answer: 2,
			},
			{
				position: 3,
				isRight: true,
				explaination: 0.1725198471,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 0.9342799392,
				answer: 4,
			},
		],
	},
	{
		question: '4mFz8355z799X11I8hzO80DdG',
		position: 8,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 0.7314428462,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 0.297969265,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 0.633986229,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 3.4667947453,
				answer: 4,
			},
		],
	},
	{
		question: '04S3T08FdxvcWR8B553H0mVu594T8',
		position: 9,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 0.1841774853,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 0.335656111,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 0.2254510038,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 0.5137591305,
				answer: 4,
			},
		],
	},
	{
		question: 'ReSh2SlR54Ip728k2608E0a',
		position: 10,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 0.1823130199,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 0.6242766042,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 1.7801801019,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 0.9432454342,
				answer: 4,
			},
		],
	},
	{
		question: 'philosophized',
		position: 11,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 0.2979579032,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 0.4089201734,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 0.3541842271,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 1.0695382289,
				answer: 4,
			},
		],
	},
	{
		question: 'A9L48ZE8PgtM3259OIITQQWKD',
		position: 12,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 1.3856895104,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 0.1699382827,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 2.6033105133,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 0.9296731948,
				answer: 4,
			},
		],
	},
	{
		question: 'granddaughters',
		position: 13,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 1.2109954909,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 1.7235709039,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 1.4870274879,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 0.1734664004,
				answer: 4,
			},
		],
	},
	{
		question: 'attractions',
		position: 14,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 1.1576206254,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 0.8199937089,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 1.1981436974,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 0.9038519846,
				answer: 4,
			},
		],
	},
	{
		question: '1421b353uqgd4t460q9584k3B1',
		position: 15,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 0.2076740758,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 1.1426860734,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 0.9595480484,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 0.302619999,
				answer: 4,
			},
		],
	},
	{
		question: 'YdFq5624h90l4ge50b54uB913',
		position: 16,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 1.0756781178,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 0.7633769364,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 0.2215727405,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 0.0318532878,
				answer: 4,
			},
		],
	},
	{
		question: '4Kd8695jU3T3yp688T50V87Q7wcUa',
		position: 17,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 0.1153491518,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 2.6105388914,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 0.5263844387,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 2.735214016,
				answer: 4,
			},
		],
	},
	{
		question: 'v82i7z325s1520fTTbIGl6',
		position: 18,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 1.4535798239,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 0.6175142934,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 0.7204115109,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 0.7469355966,
				answer: 4,
			},
		],
	},
	{
		question: 'Yb3K3l7i98ivf011i81o0L1emMEm',
		position: 19,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 0.7597468729,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 2.195213465,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 0.2652281436,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 1.9978253607,
				answer: 4,
			},
		],
	},
	{
		question: '3zY2frD3T2B988xaD7CT3SC0P0',
		position: 20,
		answer: [
			{
				position: 1,
				isRight: true,
				explaination: 0.6412809015,
				answer: 1,
			},
			{
				position: 2,
				isRight: false,
				explaination: 1.3835205069,
				answer: 2,
			},
			{
				position: 3,
				isRight: false,
				explaination: 0.5741689692,
				answer: 3,
			},
			{
				position: 4,
				isRight: false,
				explaination: 0.1543876889,
				answer: 4,
			},
		],
	},
]
