import {
	Box,
	Button,
	ButtonGroup,
	Grid,
	Heading,
	HStack,
	IconButton,
	Input,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	useQuery,
	useRadio,
	useRadioGroup,
} from '@chakra-ui/react'
import React, { useState } from 'react'
import { FaCaretUp, FaCaretDown } from 'react-icons/fa'
import RadioAnswer from './RadioAnswer'
import { useSelector } from 'react-redux'
import { useQueries } from 'react-query'
import Question from './Question'
import axios from 'axios'

const getRandomIndex = ({ question }) => ({
	randomQuestion: question.randomQuestion,
	subject: question.subject,
})

const fetchQuestion = async (params) => {
	const { data } = await axios.get(
		`http://localhost:4000/questions/${params.queryKey[1]}`
	)
	return data
}

const ModalQuestion = ({ isOpen, onClose }) => {
	const { randomQuestion, subject } = useSelector(getRandomIndex)

	const queryResults = useQueries(
		randomQuestion.map((value) => ({
			queryKey: ['questions', value],
			queryFn: fetchQuestion,
			refetchOnWindowFocus: false,
		}))
	)

	return (
		<Modal isOpen={isOpen} onClose={onClose}>
			<ModalOverlay />
			<ModalContent>
				<ModalHeader>Chủ đề 1</ModalHeader>
				<ModalCloseButton />
				<ModalBody>
					{queryResults.every((item) => item.status === 'success') && (
						<Question
							questions={queryResults.map((queryResult) => queryResult.data)}
							subject={subject}
						/>
					)}
				</ModalBody>
				<ModalFooter></ModalFooter>
			</ModalContent>
		</Modal>
	)
}

export default ModalQuestion
