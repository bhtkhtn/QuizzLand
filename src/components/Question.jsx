import React, { useState } from 'react'
import {
	useRadioGroup,
	Grid,
	Heading,
	Button,
	Box,
	Text,
} from '@chakra-ui/react'
import RadioAnswer from './RadioAnswer'

const Question = ({ questions, subject }) => {
	const [currentQuestion, setCurrentQuestion] = useState(0)
	const [currentAnswer, setCurrentAnswer] = useState('')
	const [isAnswered, setIsAnswered] = useState(false)
	const [isRightAnswer, setIsRightAnswer] = useState(false)

	const { getRadioProps, getRootProps } = useRadioGroup({
		name: 'answer',
		onChange: setCurrentAnswer,
	})

	const handleAnswer = () => {
		if (isAnswered) return
		const isRight = questions[currentQuestion].answers.find(
			(answer) => answer.position === parseInt(currentAnswer)
		)?.isRight
		if (isRight) {
			const countRightAnswer = window.localStorage.getItem(subject)
			if (!countRightAnswer) window.localStorage.setItem(subject, 1)
			else window.localStorage.setItem(subject, parseInt(countRightAnswer) + 1)
		}
		setIsAnswered(true)
		setIsRightAnswer(isRight)
	}

	const handleNextQuestion = () => {
		if (currentQuestion === 3) return
		setCurrentQuestion(currentQuestion + 1)
		setIsAnswered(false)
		setIsRightAnswer(false)
		setCurrentAnswer('')
	}

	if (!questions?.length) return null

	return (
		<>
			<Heading as='h6' size='sm' mb='5'>
				Câu {currentQuestion + 1}: {questions[currentQuestion].question}
			</Heading>
			<Grid templateColumns='repeat(2, 1fr)' gap='3' {...getRootProps()}>
				{questions[currentQuestion].answers.map((answer) => (
					<RadioAnswer
						key={answer.answer}
						{...getRadioProps({ value: answer.position })}
						pointerEvents={isAnswered ? 'none' : 'auto '}
					>
						{answer.answer}
					</RadioAnswer>
				))}
			</Grid>
			<Button mt='4' disabled={currentAnswer === ''} onClick={handleAnswer}>
				Xác nhận
			</Button>

			{isAnswered && (
				<Text my='3' color={isRightAnswer ? 'green' : 'red'}>
					Bạn trả lời {isRightAnswer ? 'đúng' : 'sai'}
				</Text>
			)}

			{isAnswered && isRightAnswer && (
				<Box mt='4'>
					Giải thích:{' '}
					{
						questions[currentQuestion].answers.find(
							(answer) => answer.position === parseInt(currentAnswer)
						)?.explaination
					}
				</Box>
			)}

			{isAnswered && (
				<Button
					mt='3'
					disabled={currentQuestion === 3 ? true : false}
					onClick={handleNextQuestion}
				>
					Câu tiếp theo
				</Button>
			)}
		</>
	)
}

export default Question
