import { combineReducers } from 'redux'
import questionReducer from './slice/question.slice'

export const rootReducer = combineReducers({
	question: questionReducer,
})
