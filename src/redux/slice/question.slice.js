import { createSlice } from '@reduxjs/toolkit'

const initialState = {
	randomQuestion: [],
  subject: 'subject1'
}

const questionSlice = createSlice({
	name: 'question',
	initialState,
	reducers: {
		setRandom(state, action) {
			state.randomQuestion = action.payload
		},
    setSubject(state, action) {
      state.subject = action.payload
    }
	},
})

const { actions, reducer } = questionSlice
export const {setRandom, setSubject} = actions
export default reducer
